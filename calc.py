import sys

def calc(in1,in2):
    try:
        a = int(in1)
        b = int(in2)
    except ValueError:
        print("One input isn't an integer!")
        return
    summation = a + b
    #print(sum)
    difference = a - b
    multiply = a * b
    try:
        divide = a / b
    except ZeroDivisionError:
        divide = "Infinity! You divided by 0"
    print("Sum is: ", end='')
    print(summation)
    print("Difference is: ", end='')
    print(difference)
    print("Product is: ", end='')
    print(multiply)
    print("Quotient is: ", end='')
    print(divide)

calc(sys.argv[1], sys.argv[2])
