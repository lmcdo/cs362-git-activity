import sys
import random

def password(strval):
    try:
        intval = int(strval)
    except ValueError:
        print("Not an integer")
        return
    string = ""
    for val in range(0, intval):
        string += chr(random.randint(33,123))
    print(string)

password(sys.argv[1])
