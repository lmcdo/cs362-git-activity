import sys

def divisors(strval):
    try:
        intval = int(strval)
    except ValueError:
        print("Not an integer")
        return
    for val in range(2, intval):
        if (intval % val) == 0:
            print(val)

divisors(sys.argv[1])
